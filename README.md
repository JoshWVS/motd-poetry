Think about something other than code--read a poem a day!

`fetch_poem.py` contains all the logic for fetching the poem (from [Poetry
Foundation](https://www.poetryfoundation.org/poems/poem-of-the-day)) and uses
[Beautiful Soup](https://beautiful-soup-4.readthedocs.io/en/latest/);
`update_motd` is an example script suitable for `/etc/cron.daily` (or similar).
