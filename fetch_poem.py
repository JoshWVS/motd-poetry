#!/usr/bin/env python

import requests
import sys
from bs4 import BeautifulSoup

# Very simple script to extract Poetry Foundation's Poem of the Day.

POETRY_URL = "https://www.poetryfoundation.org/poems/poem-of-the-day"
final_url = POETRY_URL


def get_or_die(url):
    resp = requests.get(url)

    if not resp.ok:
        # Yes, we pollute stdout, but we use this to update /etc/motd, which is
        # a great way to report errors. ;-)
        print(
            f"Failed to fetch poem; received status code {resp.status_code} from {url}"
        )
        sys.exit(1)

    return resp.text


soup = BeautifulSoup(get_or_die(POETRY_URL), "html.parser")


def full_poem_link(tag):
    return tag.name == "a" and tag.text == "Read More"


full_poem_tag = soup.find(full_poem_link)

if full_poem_tag:
    full_poem_url = full_poem_tag.attrs["href"]
    final_url = full_poem_url
    soup = BeautifulSoup(get_or_die(full_poem_url), "html.parser")

# No error checking: if one day these filters break, it will be a test of the
# discerning reader to distinguish between error messages and earnest poetry.
title = soup.find(class_="c-feature-hd").find("h1").get_text("\n").strip()
author = soup.find(class_="c-txt_attribution").get_text().strip()
poem = soup.find(class_="o-poem").get_text("\n").strip()

print(title)
print(author)
print()
print(poem)
print()
print(f"(source: {final_url})")
